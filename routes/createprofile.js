let fs = require('fs').promises;
let Url = require('url').URL;
let mongoClient = require('mongodb').MongoClient;

exports.processRoute = async function(request, response, route){
    let dbconn;
    if (request.method === 'POST'){
        let postdata = '';

        request.on('data', function(chunk){
           postdata += chunk;
        });

        request.on('end', async function(){
            let searchParams = new URLSearchParams(postdata);

        if (searchParams.has('name') && searchParams.has('model') && searchParams.has('description') && searchParams.has('gallery')){
            try{
        let galleryArray = searchParams.getAll('gallery');
                
                dbconn = await mongoClient.connect('mongodb://127.0.0.1:27017');
                let dbo = dbconn.db('Tema-Vecka-HT22');
                

                let dbpost = {
                    name: searchParams.get('name'),
                    model: searchParams.get('model'),
                    description: searchParams.get('description'),
                    galleryImages: galleryArray
                };

                await dbo.collection('Profiles').insertOne(dbpost);
                response.writeHead(303, {'Location': '/profile?profileid=' + searchParams.get('name')});
                response.end();
            }
catch{
                response.writeHead(500, {'Content-Type': 'text/plain'});
                response.end('Error 500 - Internal server error');
            }
            finally{
                dbconn.close();
            }
        }
        else{
            response.writeHead(422, {'Content-Type': 'text/plain'});
            response.end('Error - All fields was not provided');
        }
        });
    }
    else{
        let createprofile = await fs.readFile('./templates/createprofile.html', {encoding: 'utf8'});
        dbconn = await mongoClient.connect('mongodb://127.0.0.1:27017');
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.end(createprofile);
    }


};



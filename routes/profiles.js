let fs = require('fs').promises;
let mongoClient = require('mongodb').mongoClient;
let URL = require('url').URL;

exports.processRoute = async function(request, response, route){
    let dbConnection;
    try{
        let tmpProfiles = await fs.readFile('/.teamplates/profiles.html', {encoding: 'utf8'});
        dbConnection = await mongoClient.connect('mongodb://127.0.0.1:27017');
        let dbo = dbConnection('Tema-Vecka_HT22');
        let dbres = await dbo.collection('profiles').find().toArray();
        let Name = '';
        for (let i=0; i<dbres.length; i++){
            let atag = '<a href="profilepage?currentuser=';
            atag += dbres[dbres.length-1].name;
            atag += '&profileid=';
            atag += dbres[i].name;
            atag += '"<p>';
            atag += dbres[i].name;
            atag += '</p></a>=';
            Name += atag;
        }
        tmpProfiles = tmpProfiles.replace('{{profiles}}', Name);
        tmpProfiles = tmpProfiles.replace('{{currentusername}}', dbres[dbres.length-1].name);
        
        response.writeHead(200, {'content-type': 'text/html'});
        response.end(tmpProfiles);
    }
    catch(e){
        console.log(e);
    }
    finally{
        dbConnection.close();
    }
};

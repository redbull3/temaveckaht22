let profile = require('./routes/profile.js');
let createprofile = require('./routes/createprofile.js');
let fs = require('fs').promises;

exports.processRoute = async function(request, response, route){
    if (route.length === 0){
        let main = await fs.readFile('./templates/main.html', {encoding: 'utf8'});
        response.writeHead(200, {'content-type': 'text/html'});
        response.end(main);
        
    }
    else{
        let stage = route.shift();
         if(stage === 'createprofile'){
            createprofile.processRoute(request, response, route);
        }
        else if (stage === 'profile'){
            profile.processRoute(request, response, route);
        }
        else if(stage === 'profiles'){
            profile.processRoute(request, response, route);
        }
    }
};
